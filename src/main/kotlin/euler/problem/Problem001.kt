package euler.problem

import euler.Solvable

class Problem001 : Solvable {
    override fun solve(): String {
        var sum = 0

        for (i in 1..999) {
            when {
                i % 3 == 0 -> sum += i
                i % 5 == 0 -> sum += i
            }
        }

        return "$sum"
    }
}