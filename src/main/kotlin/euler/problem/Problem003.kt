package euler.problem

import euler.Solvable
import kotlin.math.sqrt

class Problem003 : Solvable {
    override fun solve(): String {
        val num = 600851475143
        var max = 0

        for (i in (1..sqrt(num.toFloat()).toInt())) {
            if (isPrime(i) && num % i == 0.toLong()) {
                max = i
            }
        }
        return "$max"
    }
}

private fun isPrime(x: Int): Boolean {
    for (i in 2..sqrt(x.toFloat()).toInt()) {
        if (x % i == 0) return false
    }
    return true
}
