package euler.problem

import euler.Solvable

class Problem002 : Solvable {
    override fun solve(): String {
        var sum = 0
        var a = 1
        var b = 1

        while (b < 4000000) {
            when {
                b % 2 == 0 -> sum += b
            }

            val c = b
            b += a
            a = c
        }

        return "$sum"
    }
}
