package euler

import euler.problem.*

val problems: Map<Int, Solvable> = mapOf(
        1 to Problem001(),
        2 to Problem002(),
        3 to Problem003()
)

fun main(args: Array<String>) {
    for (arg in args) {
        val problemNumber = arg.toInt()
        problems[problemNumber]?.let {
            println("problem $problemNumber")
            println("\t${it.solve()}")
        } ?: println("unknown problem: $problemNumber")
    }
}
