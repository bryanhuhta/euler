package euler

interface Solvable {
    /**
     * Returns a [String] representation of the solution.
     */
    fun solve(): String
}